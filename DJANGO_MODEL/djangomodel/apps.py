from django.apps import AppConfig


class DjangomodelConfig(AppConfig):
    name = 'djangomodel'
